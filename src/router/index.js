import { createRouter, createWebHistory } from 'vue-router'
import Home from '@/views/Home.vue'
import Work from "@/views/Work";
import Portfolio from "@/views/Portfolio";
import Contacts from "@/views/Contacts";
import NotFound from "@/views/NotFound";

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/co-delam',
    name: 'Work',
    component: Work
  },
  {
    path: '/portfolio',
    name: 'Portfolio',
    component: Portfolio
  },
  {
    path: '/kontakt',
    name: 'Contacts',
    component: Contacts
  },
  {
    path: '/:pathMatch(.*)*',
    component: NotFound
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
